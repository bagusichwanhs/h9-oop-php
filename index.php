<?php

require('animal.php');
require('frog.php');
require('ape.php');


$sheep = new Animal("shaun");

echo "Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded: " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new Frog("buduk");
echo "Name: " . $kodok->name . "<br>"; // "buduk"
echo "Legs: " . $kodok->legs . "<br>"; // 2
echo "Cold Blooded: " . $kodok->cold_blooded . "<br>"; // "no"
echo "Jump: " . $kodok->lompat . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->name . "<br>"; // "kera sakti"
echo "Legs: " . $sungokong->legs . "<br>"; // 2
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>"; // "no"
echo "Yell: " . $sungokong->teriak . "<br><br>";


?>